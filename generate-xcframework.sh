rm -r ./output/
cp TheBinaryFramework.podspec ./output/

# Archive for iOS
xcodebuild -workspace TheBinaryFramework.xcworkspace archive -scheme TheBinaryFramework -destination="iOS" -archivePath ./output/tmp/device.xcarchive -derivedDataPath /tmp/ddsdk_binary_DerivedData -sdk iphoneos SKIP_INSTALL=NO

# Archive for simulator
xcodebuild -workspace TheBinaryFramework.xcworkspace archive -scheme TheBinaryFramework -destination="iOS Simulator" -archivePath ./output/tmp/simulator.xcarchive -derivedDataPath /tmp/ddsdk_binary_DerivedData -sdk iphonesimulator SKIP_INSTALL=NO

# Build xcframework with two archives
xcodebuild -create-xcframework -framework ./output/tmp/device.xcarchive/Products/Library/Frameworks/TheBinaryFramework.framework -framework ./output/tmp/simulator.xcarchive/Products/Library/Frameworks/TheBinaryFramework.framework -output ./output/TheBinaryFramework.xcframewor

rm -r ./output/tmp
