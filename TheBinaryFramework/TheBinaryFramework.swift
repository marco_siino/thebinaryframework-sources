//
//  TheBinaryFramework.swift
//  TheBinaryFramework
//
//  Created by Marco Siino on 21/01/2020.
//  Copyright © 2020 Marco Siino. All rights reserved.
//

import Foundation
import TheHelperDependency

public class TheBinaryFramework {
    public init() { }
    
    public func binaryMethod(str: String) -> String {
        return "Hello \(str) from binary framework!"
    }
    
    public func useAwesomeDependency(str: String) {
        let c = AwesomeDependency()
        print(c.awesomeMethod(str: str))
    }
}
