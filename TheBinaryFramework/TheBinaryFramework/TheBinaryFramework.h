//
//  TheBinaryFramework.h
//  TheBinaryFramework
//
//  Created by Marco Siino on 21/01/2020.
//  Copyright © 2020 Marco Siino. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for TheBinaryFramework.
FOUNDATION_EXPORT double TheBinaryFrameworkVersionNumber;

//! Project version string for TheBinaryFramework.
FOUNDATION_EXPORT const unsigned char TheBinaryFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TheBinaryFramework/PublicHeader.h>


